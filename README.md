# Selecciona un color #

Seleccionador de colores en base a distintos parámetros, para ser usados en los estilos de elementos HTML. Esta herramienta se presta como apoyo a los desarrolladores front-end.  

## Tipos de selección ##

### Por valor hexadecimal ###

Se puede ingresar un valor hexadecimal para mostrar el color correspondiente a dicho valor. 

### Por colores ###

Se puede ingresar o seleccionar los valores de los colores rojo, verde y azul para mostrar el color correspondiente a la combinación de dichos valores.

### Por nombre ###

Se puede ingresar o seleccionar el nombre de un color para mostrarlo. 

## Uso ##

Esta herramienta es accesible desde [este enlace](https://crismvp3200.github.io/selectcolor/). 

## Versión ##

1.0.0

## Contacto 

* Bitbucket: [https://bitbucket.org/CrisMVP3200/](https://bitbucket.org/CrisMVP3200/) 
* GitHub: [https://github.com/CrisMVP3200](https://github.com/CrisMVP3200)
* Correo: [cristiancajiaos@gmail.com](cristiancajiaos@gmail.com)